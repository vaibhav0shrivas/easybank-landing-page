const toggle_btn = document.getElementById('menuToggle');
const menu = document.getElementById('header-nav');

toggle_btn.addEventListener('click', () => {
    if (menu.style.opacity == 0) {
        menu.style.opacity = 1;
    } else {
        menu.style.opacity = 0;
    }
});